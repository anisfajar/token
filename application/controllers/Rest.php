<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH.'/libraries/jwt/src/BeforeValidException.php');
require_once(APPPATH.'/libraries/jwt/src/SignatureInvalidException.php');
require_once(APPPATH.'/libraries/jwt/src/ExpiredException.php');
require_once(APPPATH.'/libraries/jwt/src/JWT.php');

// require APPPATH . './vendor/autoload.php';
require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;
use \Firebase\JWT\JWT;

class Rest extends REST_Controller {
    private $secretkey = "401b09eab3c013d4ca54922bb802bec8fd5318192b0a75f201d8b3727429090fb337591abd3e44453b954555b7a0812e1081c39b740293f765eae731f5a65ed1";

    public function __construct($config = 'rest'){
        parent::__construct($config);
        date_default_timezone_set('Asia/Jakarta');
        $this->load->model('m_user');
    }

    public function generate_post(){ //login tanpa cek database
        $date     = new DateTime();
        $username = trim($this->post('nm_usr','true'));
        $password = trim($this->post('pass','true'));
        
        if ($username == "" || $password == "") {
            $this->response([
                'status'    => FALSE,
                'code'      => 400,
                'result'    => 'Maaf, data tidak boleh kosong'
            ],REST_Controller::HTTP_BAD_REQUEST);
        } else {
            $payload['nm_usr'] = md5($username);
            $payload['pass']   = md5($password);
            $payload['iat']    = $date->getTimestamp(); //create waktu
            $payload['exp']    = $date->getTimestamp() + 60; //1 menit
            $output['token']   = JWT::encode($payload,$this->secretkey);
            return $this->response([$output],REST_Controller::HTTP_OK);
        }
    }
    
    public function viewtokenfail($username){
        $this->response([
          'status'      => FALSE,
          'username'    => $username,
          'message'     => 'Invalid!'
          ],REST_Controller::HTTP_BAD_REQUEST);
    }

    public function cektoken(){
        $jwt = $this->input->get_request_header('Authorization');
        try {
            $decode = JWT::decode($jwt,$this->secretkey,array('HS256'));
            if ($this->m_user->is_valid_num($decode->nm_usr AND $decode->pass)>0) {
                return true;
            }
        } catch (Exception $e) {
            $res = ['result' => 'Token salah'];
            echo json_encode($res);
            exit();
        }
    }

    // public function generate_post(){ //login cek database
    //     $date       = new DateTime();
    //     $username   = trim($this->post('nm_usr','true'));
    //     $password   = trim(md5($this->post('pass','true')));
    //     $datauser   = $this->m_user->is_valid($username,$password);
    //     $countdata  = count($datauser);
        
    //     if ($countdata > 0) {
    //         $payload['nm_usr'] = $datauser->nm_usr;
    //         $payload['pass']   = $datauser->pass;
    //         $payload['iat']    = $date->getTimestamp(); //create waktu
    //         $payload['exp']    = $date->getTimestamp() + 60; //1 menit
    //         $output['token']   = JWT::encode($payload,$this->secretkey);
    //         return $this->response($output,REST_Controller::HTTP_OK);
    //     } else {
    //         $this->viewtokenfail($username);
    //     }
    // }

}