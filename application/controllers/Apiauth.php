<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH.'/libraries/jwt/src/BeforeValidException.php');
require_once(APPPATH.'/libraries/jwt/src/SignatureInvalidException.php');
require_once(APPPATH.'/libraries/jwt/src/ExpiredException.php');
require_once(APPPATH.'/libraries/jwt/src/JWT.php');

require APPPATH . 'third_party/REST_Controller.php';
require APPPATH . 'third_party/Format.php';

use Restserver\Libraries\REST_Controller;
use \Firebase\JWT\JWT;

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

class Apiauth extends REST_Controller {
    public function __construct() {
        parent::__construct();
        // Load these helper to create JWT tokens
        $this->load->helper(['jwt', 'authorization']); 
    }

    public function hello_get() {
        $tokenData = 'Hello World!';
        // Create a token
        $token = AUTHORIZATION::generateToken($tokenData);
        // Set HTTP status code
        $status = parent::HTTP_OK;
        // Prepare the response
        $response = ['status' => $status, 'token' => $token];
        // REST_Controller provide this method to send responses
        $this->response($response, $status);
    }

    public function login_post() {
        $dummy_user = [
            'username' => 'admin',
            'password' => 'admin'
        ];
        
        $username = $this->post('username');
        $password = $this->post('password');
        
        if ($username == $dummy_user['username'] && $password == $dummy_user['password']) {
            $token = AUTHORIZATION::generateToken(['username' => $dummy_user['username']]);
            $status = parent::HTTP_OK;
            $response = ['status' => $status, 'token' => $token];
            $this->response($response, $status);
        } else {
            $this->response(['msg' => 'Invalid username or password!'], parent::HTTP_NOT_FOUND);
        }
    }

    private function verify_request() {
        $headers = $this->input->request_headers();
        $token = $headers['Authorization'];
        try {
            $data = AUTHORIZATION::validateToken($token);
            if ($data == false) {
                $status = parent::HTTP_UNAUTHORIZED;
                $response = ['status' => $status, 'msg' => 'Unauthorized Access!'];
                $this->response($response, $status);
                exit();
            } else {
                return $data;
            }
        } catch (Exception $e) {
            $status = parent::HTTP_UNAUTHORIZED;
            $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
            $this->response($response, $status);   
        }
    }

    public function getmedata_post() {
        $data = $this->verify_request();
        if ($data) {
            $status = parent::HTTP_OK;
            $response = ['status' => $status, 'data' => $data];
            $this->response($response, $status);
        } else {
            $status = parent::HTTP_UNAUTHORIZED;
            $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
            $this->response($response, $status);   
        }
    }
}