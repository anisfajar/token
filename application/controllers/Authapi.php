<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH.'/libraries/jwt/src/BeforeValidException.php');
require_once(APPPATH.'/libraries/jwt/src/SignatureInvalidException.php');
require_once(APPPATH.'/libraries/jwt/src/ExpiredException.php');
require_once(APPPATH.'/libraries/jwt/src/JWT.php');

// require APPPATH . './vendor/autoload.php';
require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;
use \Firebase\JWT\JWT;

class Authapi extends REST_Controller {

    private $secretkey = "401b09eab3c013d4ca54922bb802bec8fd5318192b0a75f201d8b3727429090fb337591abd3e44453b954555b7a0812e1081c39b740293f765eae731f5a65ed1";

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
        $this->load->model('m_user');
        $this->load->model('m_wilayah');
    }

    function index_post() {
        $auth = $this->post('Authorization');
        $usr  = $this->post('nm_usr');
        $pass = $this->post('pass');

        if ($auth == "" || $usr == "" || $pass == "") {
            $statusbad = parent::HTTP_BAD_REQUEST;
            return $this->response([
                'status'    => false,
                'code'      => $statusbad,
                'result'    => 'Maaf, inputan tidak boleh kosong'
            ],REST_Controller::HTTP_BAD_REQUEST);
        } else {
            try {
                $decode = JWT::decode($auth,$this->secretkey,array('HS256'));
                if ($decode->nm_usr == md5($usr) && $decode->pass == md5($pass)) {
                    $statusok  = parent::HTTP_OK;
                    $datasam = $this->m_wilayah->getalldatasam()->result();
                    return $this->response([
                        'status'    => TRUE,
                        'code'      => $statusok,
                        'result'    => 'Success'
                    ]);
                } else {
                    $statusbad = parent::HTTP_BAD_REQUEST;
                    return $this->response([
                        'status'    => FALSE,
                        'code'      => $statusbad,
                        'result'    => 'Invalid username / password'
                    ]);
                }
            } catch (Exception $e) {
                $statusbad = parent::HTTP_BAD_REQUEST;
                return $this->response([
                    'status'    => FALSE,
                    'code'      => $statusbad,
                    'result'    => 'Token tidak berlaku'
                ]);
                exit();
            }
        }
    }

    // function index_post() {
    //     $auth = $this->post('Authorization');
    //     $usr  = $this->post('nm_usr');
    //     $pass = $this->post('pass');

    //     if ($auth == "" || $usr == "" || $pass == "") {
    //         $response = ['result' => "Maaf, data tidak boleh kosong"];
    //         $this->response($response, 400);
    //     } else {
    //         try {
    //             $decode = JWT::decode($auth,$this->secretkey,array('HS256'));
    //             if ($decode->nm_usr == $usr && $decode->pass == md5($pass)) {
    //                 $res = ['result' => 'Berhasil'];
    //                 return $this->response($res);
    //                 // echo "berhasil";
    //             } else {
    //                 $res = ['result' => 'Gagal'];
    //                 return $this->response($res);
    //                 // echo "gagal";
    //             }
    //         } catch (Exception $e) {
    //             $res = ['result' => 'Token tidak berlaku'];
    //             return $this->response($res);
    //             // echo 'token expired';
    //             exit();
    //         }
    //     }
    // }
}