<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class M_wilayah extends CI_Model {
	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	function get_allwilayah() {
		$level = $this->session->userdata('level');		
		if ($level=='1') {
			$this->db->select('*');
			$this->db->from('kodewilayah');
			// $this->db->where('flag_aktif','TRUE');
			$this->db->where('kodewilayah !=', 'SAMDISHUB');
			$this->db->where('kodewilayah !=', 'DEVEL');
			// $this->db->where('namawilayah !=', 'JTO KABUPATEN JEPARA');
			// $this->db->not_like('namawilayah', 'JTO');
			$this->db->order_by('namawilayah','asc');
		} else {
			//$wilayah = $this->session->userdata('wilayah');	
			$this->db->select('*');
			$this->db->from('kodewilayah');
			$this->db->where('kodewilayah !=', 'SAMDISHUB');
			$this->db->where('kodewilayah !=', 'DEVEL');
			// $this->db->where('kodewilayah !=', 'JTDEV');
			// $this->db->not_like('namawilayah', 'JTO');
			$this->db->where('flag_aktif','TRUE');
			$this->db->order_by('namawilayah','asc');			
		}	
		return $this->db->get();
		// print_r($this->db->get()->result());
	}

	function get_allwilayah2() {
		$level = $this->session->userdata('level');		
		if ($level=='1') {
			$this->db->select('*');
			$this->db->from('kodewilayah');
			// $this->db->where('flag_aktif','TRUE');
			$this->db->where('kodewilayah !=', 'SAMDISHUB');
			$this->db->order_by('namawilayah','asc');
		} else {
			//$wilayah = $this->session->userdata('wilayah');	
			$this->db->select('*');
			$this->db->from('kodewilayah');
			$this->db->where('kodewilayah !=', 'SAMDISHUB');
			$this->db->where('flag_aktif','TRUE');
			$this->db->order_by('namawilayah','asc');			
		}	
		return $this->db->get();
		// print_r($this->db->get()->result());
	}

	function get_wilayahwhere($wilayah)
	{
		$level = $this->session->userdata('level');
		if ($level=='1') {
			$query = "SELECT * FROM kodewilayah WHERE kodewilayah = '$wilayah' AND namawilayah NOT LIKE '%JTO%' ";
		} else {
			$query = "SELECT * FROM kodewilayah WHERE kodewilayah = '$wilayah' AND namawilayah NOT LIKE '%JTO%' and flag_aktif = 'TRUE' ";
		}

		return $this->db->query($query)->row();
	}

	function lap_wilayah() {
		$level = $this->session->userdata('level');		
		$kode = array('SAMDISHUB', 'DEVEL', 'PUSAT');
		if ($level=='1') {
			$this->db->select('*');
			$this->db->from('kodewilayah');
			$this->db->where('kodewilayah !=', 'JTDEV');
			$this->db->where_not_in('kodewilayah', $kode);
			$this->db->order_by('namawilayah','asc');
		} else {
			//$wilayah = $this->session->userdata('wilayah');	
			$this->db->select('*');
			$this->db->from('kodewilayah');
			//$this->db->where('kodewilayah', $wilayah);
			$this->db->where_not_in('kodewilayah', $kode);;
			$this->db->not_like('namawilayah','JTO');
			$this->db->where('flag_aktif','TRUE');
			$this->db->order_by('namawilayah','asc');			
		}	
		return $this->db->get();
	}

	function get_wilayahold($id) {
		$this->db->select('*');
		$this->db->from('kodewilayah');
		$this->db->where('idx', $id);
		
		return $this->db->get();
	}

	function get_wilayah($kode) {
		$this->db->select('*');
		$this->db->from('kodewilayah');
		$this->db->where('kodewilayah', $kode);

		return $this->db->get();
	}

	function get_wilayahnopusat() {
		$this->db->select('*');
		$this->db->from('kodewilayah');
		$this->db->where('kodewilayah <>', 'PUSAT');
		$this->db->where('kodewilayah <>', 'SAMDISHUB');
		$this->db->where('flag_aktif','TRUE');
		$this->db->not_like('kodewilayah', 'JT','after');
		$this->db->order_by('namawilayah','asc');			
		
		return $this->db->get();
	}

	function get_samperwilayah($kode){
		$this->db->select('*');
		$this->db->from('sam a');
		$this->db->join('kodewilayah b','a.kode_wilayah=b.kodewilayah','LEFT');
		$this->db->where('a.kode_wilayah',$kode);

		return $this->db->get();
	}

	function get_sam(){
		$this->db->select('*');
		$this->db->from('sam a');
		$this->db->join('kodewilayah b','a.kode_wilayah=b.kodewilayah','INNER');
		$this->db->where('a.kode_wilayah != ','DEVEL');
		$this->db->where('a.kode_wilayah != ','PUSAT');

		return $this->db->get();
	}

	function getalldatasam(){
		$this->db->select('b.namawilayah,a.sam_id');
		$this->db->from('sam a');
		$this->db->join('kodewilayah b','a.kode_wilayah=b.kodewilayah');
		$this->db->where('kode_wilayah !=','PUSAT');
		$this->db->where('kode_wilayah !=','DEVEL');

		return $this->db->get();
	}

	function save_wilayah($data) {
	   	$this->db->insert('kodewilayah', $data);
       	return TRUE;		       	    
	}

	function update_wilayah($id, $data) {
		$this->db->where('idx', $id);
		$this->db->update('kodewilayah', $data);
	}

	function delete_wilayah($idx, $idw) {
		$this->db->where('idx', $idx);
		$this->db->where('kodewilayah', $idw);
		$this->db->delete('kodewilayah');
	}

	function flag_wilayah($id, $data) {
		$this->db->where('idx', $id);
		$this->db->update('kodewilayah', $data);
		return TRUE;		       	    
	}	
}

/* End of File M_wilayah.php */
/* Location: ./application/model/ */
