<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class M_user extends CI_Model {
	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function is_valid($username,$pass){
		$this->db->select('*');
		$this->db->from('tbl_web_user');
		$this->db->where('nm_usr',$username);
		$this->db->where('pass',$pass);
		$query = $this->db->get();
		return $query->row();
	}

	public function is_valid_num($username,$pass){
		$this->db->select('*');
		$this->db->from('tbl_web_user');
		$this->db->where('nm_usr',$username);
		$this->db->where('pass',$pass);
		$query = $this->db->get();
		return $query->num_rows();
	  }

	function check_user_accountclient($username, $password) {
		$this->db->select('*');
		$this->db->from('tbl_web_user');
		$this->db->where('nm_usr', $username);
		$this->db->where('pass', $password);

		return $this->db->get();
	}

	function get_alluser() {
		$level = $this->session->userdata('level');
		if ($level=='1') {
			$this->db->select('a.*, b.namawilayah');
			$this->db->from('tbl_web_admin a');
			$this->db->join('kodewilayah b', 'a.kodewilayah = b.kodewilayah', 'left');
			$this->db->where('a.nm_usr != ','adminblue');
			$this->db->order_by('a.kodegrup','asc');		
			$this->db->order_by('a.nm_usr','asc');		
			$this->db->order_by('a.kodewilayah','asc');	
		} else {
			//$wilayah = $this->session->userdata('wilayah');	
			$this->db->select('a.*, b.namawilayah');
			$this->db->from('tbl_web_admin a');
			$this->db->join('kodewilayah b', 'a.kodewilayah = b.kodewilayah', 'left');
			//$this->db->where('a.kodewilayah', $wilayah);
			$this->db->where('a.kodegrup', $groupid);
			$this->db->order_by('a.kodegrup','asc');	
			$this->db->order_by('a.kodewilayah','asc');	
			$this->db->order_by('a.nm_usr','asc');				
		}	
		return $this->db->get();
	}

	function get_userold($iduser) {
		$this->db->select('a.*, b.namawilayah');
		$this->db->from('tbl_web_admin a');
		$this->db->join('kodewilayah b', 'a.kodewilayah = b.kodewilayah', 'left');
		$this->db->where('a.id_usr', $iduser);		

		return $this->db->get();
	}

	function get_username($user) {
		$this->db->select('count(*) as cek');
		$this->db->from('tbl_web_admin');
		$this->db->where('nm_usr',$user);
		
		return $this->db->get();
	}

	function save_user($data) {
	   	$this->db->insert('tbl_web_admin', $data);
 	}

	function update_user($id, $data) {
		$this->db->where('id_usr', $id);
		$this->db->update('tbl_web_admin', $data);
	}

	function delete_user($id) {
		$this->db->where('id_usr', $id);
		$this->db->delete('tbl_web_admin');
	}

	function save_log($log) {
	   	$this->db->insert('tbl_log', $log);
 	}

	function get_loguser() {
		$this->db->select('a.*, b.nm_usr');
		$this->db->from('tbl_log a');
		$this->db->join('tbl_web_admin b', 'a.id_usr = b.id_usr', 'left');
		//$this->db->like('a.tanggal', $tanggal,'after');
		$this->db->order_by('a.tanggal','DESC');
		$this->db->limit(500);		

		return $this->db->get();
	}

	//$this->db->like('field', 'value', 'before'); // sql: WHERE `field` LIKE '%value'
	//$this->db->like('field', 'value', 'after'); // sql: WHERE `field` LIKE 'value%'
	//$this->db->like('field', 'value', 'both'); // sdl: WHERE `field` LIKE '%value%'

	function get_pangkat() {
		$this->db->select('*');
		$this->db->from('pangkat');
		$this->db->order_by('idx','ASC');	

		return $this->db->get();
	}

	function get_levelkompetensi() {
		$this->db->select('*');
		$this->db->from('levelkompetensi');
		$this->db->order_by('idx','ASC');	

		return $this->db->get();
	}

	// function update_otp($nm_usr,$pass){
	// 	$sql = $this->db->query("UPDATE tbl_web_user SET otp = '123' WHERE nm_usr = '$nm_usr' AND pass = '$pass' ");

	// 	return $sql;
	// }

	function update_otp($nm_usr,$pass,$data) 
	{
		$this->db->where('nm_usr', $nm_usr);
		$this->db->where('pass', $pass);
		$this->db->update('tbl_web_user', $data);
	}
}

/* End of file M_user.php */
/* Location: ./application/model/M_user.php */