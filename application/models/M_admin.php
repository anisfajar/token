<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class M_admin extends CI_Model {
	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	function check_user_account($username, $password) {
		$this->db->select('*');
		$this->db->from('tbl_web_admin');
		$this->db->where('nm_usr', $username);
		$this->db->where('pass', $password);

		return $this->db->get();
	}
	
	function get_user($username) {
		$this->db->select('*');
		$this->db->from('tbl_web_admin');
		$this->db->where('nm_usr', $username);

		return $this->db->get();
	}

	function get_group($username, $password) {
		$this->db->select('kodegrup');
		$this->db->from('tbl_web_admin');
		$this->db->where('nm_usr', $username);
		$this->db->where('pass', $password);		

		return $this->db->get();
	}

	function select_detail() {
		$nama = $this->session->userdata('username');
		// $this->db->select('*');
		// $this->db->from('tbl_web_admin');
		// $this->db->where('nm_usr', $nama);

        $this->db->select('a.*, b.namawilayah');
        $this->db->from('tbl_web_admin a');
        $this->db->join('kodewilayah b', 'a.kodewilayah = b.kodewilayah', 'left');
        $this->db->where('a.nm_usr', $nama);
        $this->db->order_by('a.nm_usr', 'asc');

		return $this->db->get();
	}

	function get_pwdold($username) {
		$this->db->select('pass');
		$this->db->from('tbl_web_admin');
		$this->db->where('nm_usr', $username);

		return $this->db->get();		
	}

	function update_data_password() {
		$xnama 		= trim($this->session->userdata('username'));
		$password2 	= trim($this->input->post('password2'));

		if (!empty($password2)) { // Jika Password Diisi / Change Password
			$data = array(
		    		'pass' 		=> md5(trim($this->input->post('password2'))),
				);
		
			$this->db->where('nm_usr', $xnama);
			$this->db->update('tbl_web_admin', $data);
		} 
	}	

}
/* Location: ./application/model/M_admin.php */
